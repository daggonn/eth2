pragma solidity ^0.4.18;
contract UserCard {
    // Defines a new type with two fields.
    struct User {
        string UserName;
        string Email;
        string Phone;
        string Addr;
    }
    address owner;
    mapping (address => User) Users;
    function UserCard() public {
        owner = msg.sender;
    }


    function SetDataUser(string UserName, string Email, string Phone, string Addr) public {
        require(msg.sender == owner);
        Users[msg.sender] = User(UserName, Email, Phone, Addr);
    }

    function GetDataUser(address walletUser) public view returns (string, string, string, string){
        return (Users[walletUser].UserName, Users[walletUser].Email, Users[walletUser].Phone, Users[walletUser].Addr);
    }
}
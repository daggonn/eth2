import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import WelcomeForm from './components/WelcomeForm';
import Report from './components/Report';
import {HashRouter, Switch, Route, Link } from 'react-router-dom';
import Menu from './components/Menu';
const AppAll = () => (
  <div className="App">
      <Menu/>
      <Switch>
        <Route exact path='/'  component={WelcomeForm} />
        <Route path="/search" component={Report} />
      </Switch>

 </div>
);

class App extends Component {
  state = {
    mounted : true
  };

  render() {
    return (     
      <HashRouter>
        <AppAll />
      </HashRouter>
    );
  }
}

export default App;

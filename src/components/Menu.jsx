import React from 'react';
import {Link } from 'react-router-dom';
class Menu extends React.Component{ 
    render(){
        return(
            <nav>
               <div className="card text-center">
                    <div className="card-header">
                        <ul className="nav nav-pills card-header-pill">
                            <li className="nav-item"><Link className="nav-link" to='/'>Home</Link></li>
                            <li className="nav-item"><Link className="nav-link" to='/search'>Search</Link></li>
                        </ul>
                    </div>
                </div>
           </nav>
        )
    }
}

export default Menu;
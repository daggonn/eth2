import React from 'react';
import lightwallet from 'eth-lightwallet';
import Web3 from 'web3';
import HookedWeb3Provider from 'hooked-web3-provider';
import async from 'async';
import encryption from 'eth-lightwallet';
import promisify from 'promisify';
import Profile from './Profile';
import PreviewProfile from './PreviewProfile';
//const { encryption, keystore, upgrade } = require('eth-lightwallet');



async function createKeystoreObj(settings) {
    const keystore = lightwallet.keystore;
    // generate a new BIP32 12-word seed
    const seedPhrase = "dress owner crew reason basket matrix family quarter mimic amused faculty winner";

    // Create new keystore
    const options = {
        password: '123',
        seedPhrase,
        hdPathString: "m/0'/0'/0'"
    };
    keystore.pCreateVault = promisify(keystore.createVault);
    let keystoreObj = await keystore.pCreateVault(options);
    keystoreObj.pKeyFromPassword = promisify(keystoreObj.keyFromPassword);
    const pwDerivedKey = await keystoreObj.pKeyFromPassword("123");

    keystoreObj.generateNewAddress(pwDerivedKey, 1);
    // Create new public key for the server
    const address = keystoreObj.getAddresses()[0];
    const serverPublicKey = encryption.addressToPublicEncKey(keystoreObj, pwDerivedKey, address);

    return { keystoreObj, serverPublicKey };
}


class WelcomeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            wallet: '',
            balance: 0
        }
        
    }
    static web3 = new Web3();
    static global_keystore;

    setWeb3Provider(keystore) {
        var web3Provider = new HookedWeb3Provider({
            host: "https://ropsten.shelterzoom.com:7544/",
            transaction_signer: keystore
        });
        
        WelcomeForm.web3.setProvider(web3Provider);
    }

    newAddresses(password) {

        if (password == '') {
            password = prompt('Enter password to retrieve addresses', 'Password');
        }

        WelcomeForm.global_keystore.keyFromPassword(password, (err, pwDerivedKey) => {

            WelcomeForm.global_keystore.generateNewAddress(pwDerivedKey, 1);

            this.getBalances();
        });
    }

    getBalances() {

        var addresses = WelcomeForm.global_keystore.getAddresses();
        document.getElementById('addr').innerHTML = 'Retrieving addresses...';

        async.map(addresses, WelcomeForm.web3.eth.getBalance, (err, balances) => {
            async.map(addresses, WelcomeForm.web3.eth.getTransactionCount, (err, nonces) => {
                document.getElementById('addr').innerHTML = ''
                for (var i = 0; i < addresses.length; ++i) {
                    document.getElementById('addr').innerHTML += '<div>' + addresses[i]
                        + ' (Bal: ' + (balances[i] / 1.0e18)
                        + ' ETH, Nonce: ' + nonces[i] + ')' + '</div>';
                    this.setState({ balance: (balances[i] / 1.0e18), wallet: addresses[i] });
                }
            })
        });
       

    }

    setSeed() {
        var password = prompt('Enter Password to encrypt your seed', 'Password') || '';
        if(password == '' || password == null){
            return;
        }

        lightwallet.keystore.createVault({
            password: password,
            seedPhrase: document.getElementById('seed').value,
            //random salt 
            hdPathString: "m/0'/0'/0'"
        }, (err, ks) => {
            WelcomeForm.global_keystore = ks;
            document.getElementById('seed').value = ''
            this.newAddresses(password);
            this.setWeb3Provider(WelcomeForm.global_keystore);

            this.getBalances();
        });
    }

    newWallet() {
        var extraEntropy = document.getElementById('userEntropy').value;
        document.getElementById('userEntropy').value = '';
        var randomSeed = lightwallet.keystore.generateRandomSeed(extraEntropy);

        var infoString = 'Your new wallet seed is: "' + randomSeed +
            '". Please write it down on paper or in a password manager, you will need it to access your wallet. Do not let anyone see this seed or they can take your Ether. ' +
            'Please enter a password to encrypt your seed while in the browser.'
        var password = prompt(infoString, 'Password');


        lightwallet.keystore.createVault({
            password: password,
            seedPhrase: randomSeed,
            //random salt 
            hdPathString: "m/0'/0'/0'"
        }, (err, ks) => {
            WelcomeForm.global_keystore = ks;
            this.newAddresses(password);
            this.setWeb3Provider(WelcomeForm.global_keystore);
            this.getBalances();
        });
    }
    render() {
        return (
            <div className="container mt-5">
                <div className="text-left">Please input random text for generate new wallet</div>
                <div className="input-group mb-3">
                    <input className="form-control"  placeholder="Random phrase" aria-label="Random phrase" aria-describedby="basic-addon2"  type="text" id="userEntropy" />
                    <button className="btn btn-primary btn-sm" onClick={this.newWallet.bind(this)}>Create Wallet</button>
                </div>
                <div  className="text-left">Please input your seed phrase for get information about wallet</div>
                <div className="input-group mb-3" >
                    <input className="form-control" placeholder="Seed Phrase for Wallet" aria-label="Seed phrase for wallet" aria-describedby="basic-addon2" type="text" id="seed" />
                    <button className="btn btn-primary btn-sm" onClick={this.setSeed.bind(this)}>Check balance</button>
                </div>
                <div className="text-left">Action results:</div>
                <div className="alert alert-primary" id="addr"></div>
                 {this.state.balance !== 0 ? <Profile data = {this.state} /> : <PreviewProfile />} 
                {/* <Profile data = {this.state} /> */}
            </div>


        )

    }
}
export default WelcomeForm;


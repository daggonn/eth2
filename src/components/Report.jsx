import React from 'react';
import WelcomeForm from './WelcomeForm';
import lightwallet from 'eth-lightwallet';
import Web3 from 'web3';
import BigNumber from 'bignumber.js'


class Report extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            UserName : '',
            Email : '',
            Phone : '',
            Address: '',
            Wallet: '',
            Contract :'',
            answer:[],
            ...props
        };
        this.handleChange = this.handleChange.bind(this);
        this.web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.shelterzoom.com:7544/")) 
    }
    
    getUserData(){
        this.web3.eth.defaultAccount = this.state.Wallet;
        var Contract = this.web3.eth.contract([ { "constant": false, "inputs": [ { "name": "UserName", "type": "string" }, { "name": "Email", "type": "string" }, { "name": "Phone", "type": "string" }, { "name": "Addr", "type": "string" } ], "name": "SetDataUser", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "constant": true, "inputs": [ { "name": "walletUser", "type": "address" } ], "name": "GetDataUser", "outputs": [ { "name": "", "type": "string" }, { "name": "", "type": "string" }, { "name": "", "type": "string" }, { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ]);
        var AtContract = Contract.at(this.state.Contract);
        
       if(this.state.Contract){
           var answer = AtContract.GetDataUser.call(this.state.Wallet);

            this.setState({
                UserName : answer[0],
                Email : answer[1],
                Phone : answer[2],
                Address: answer[3],
                answer
            });
            
             console.log(answer);
        }
    }
    
    handleChange(event){
        this.setState({[event.target.name] : event.target.value});
    }

    handleChangeContact(event){
        this.setState({Contract:event.target.value})
    }

    render() {
        return(
     <div className="container mt-5">
            <div className="text-left">Please input your wallet for check inforamtion:</div>
            <div className="input-group mb-3">
                <input className="form-control"  placeholder="Wallet" aria-label="Wallet" aria-describedby="basic-addon2"
                value={this.state.Wallet} 
                onChange={this.handleChange} 
                name="Wallet"
                type="text" id="userEntropy" />
                <button className="btn btn-primary btn-sm" onClick={this.getUserData.bind(this)}>Get Data</button>
            </div>
            {!!this.state.answer.length && !this.state.answer.filter(e=>!!e).length && 
                    <div className="container mt-5">
                        <div className="alert alert-danger" role="alert">
                          No data for this Wallet
                        </div>
                    </div>}
            <div className="text-left">Please select type contract:</div>
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <label className="input-group-text">Contract</label>
                </div>
               <select id="contractList" className="custom-select" onChange={this.handleChangeContact.bind(this)} value={this.state.Contract}>
                    <option value=""></option>
                    <option value="0xf12815648f4d7a1edd02cd7e0cc1100e001612ad">Contract without require</option>
                    <option value="0x7c3105edcf5c0e80edfac6a912f567b62288fee9">Contract with require</option>
                    <option value="0x7d2f4c97ab3b626f91ed365079adf5ed4cdaf0e1">Contract with require and wallet address</option>
               </select>
            </div>
            <div className="input-group mb-3">
                <input className="form-control" type="text" 
                    placeholder="UserName" aria-label="UserName" aria-describedby="basic-addon2" 
                    name="UserName" value={this.state.UserName} 
                    onChange={this.handleChange}/>
            </div>
            <div className="input-group mb-3">
                <input className="form-control" type="text" 
                    placeholder="E-mail" aria-label="E-mail" aria-describedby="basic-addon2" 
                    name="Email" value={this.state.Email} 
                    onChange={this.handleChange}/>
            </div>
            <div className="input-group mb-3">
                <input className="form-control" type="text" 
                    placeholder="Phone number" aria-label="Phone number" aria-describedby="basic-addon2" 
                    name="Phone" value={this.state.Phone}
                    onChange={this.handleChange}/>
            </div>
            <div className="input-group mb-3">
                <input className="form-control" type="text" 
                    placeholder="Address" aria-label="Address" aria-describedby="basic-addon2" 
                    name="Address" value={this.state.Address} 
                    onChange={this.handleChange}/>                   
            </div>

        </div>

        )
    }
}
export default Report;
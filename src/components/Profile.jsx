import React from 'react';
import WelcomeForm from './WelcomeForm';
import lightwallet from 'eth-lightwallet';
import Web3 from 'web3';
import BigNumber from 'bignumber.js'

class Profile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            UserName : '',
            Email : '',
            Phone : '',
            Address: '',
            ...props
        };
        this.handleChange = this.handleChange.bind(this);
        this.web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.shelterzoom.com:7544/")) 
    }
    
    setUserData(){
        const wallet = '0x83E159f7308f5b9Ce1DC74Cb9D315bBd20372057'
       // this.web3.eth.defaultAccount = wallet;
       this.web3.personal.unlockAccount(
        wallet,
        '1qaz2wsx');
        var Contract = this.web3.eth.contract([ { "constant": false, "inputs": [ { "name": "UserName", "type": "string" }, { "name": "Email", "type": "string" }, { "name": "Phone", "type": "string" }, { "name": "Addr", "type": "string" } ], "name": "SetDataUser", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "constant": true, "inputs": [ { "name": "walletUser", "type": "address" } ], "name": "GetDataUser", "outputs": [ { "name": "", "type": "string" }, { "name": "", "type": "string" }, { "name": "", "type": "string" }, { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ]);
        var AtContract = Contract.at('0xb1057c568489e3d181e0166309d6a6475469d3bc');  
        var answer = AtContract.SetDataUser.sendTransaction(this.state.UserName
            , this.state.Email
            , this.state.Phone
            , this.state.Address
            , { from: wallet});
        
        console.log(answer);
    }

    handleChange(event){
        this.setState({[event.target.name] : event.target.value});
    }

    render() {
        return(
            <div className="container mt-5">
                <div className="alert alert-success" role="alert">
                please set data about himself and click button save.
                </div>
                <div className="input-group mb-3">
                    <input className="form-control" type="text" 
                        placeholder="UserName" aria-label="UserName" aria-describedby="basic-addon2" 
                        name="UserName" value={this.state.UserName} 
                        onChange={this.handleChange}/>
                </div>
                <div className="input-group mb-3">
                    <input className="form-control" type="text" 
                        placeholder="E-mail" aria-label="E-mail" aria-describedby="basic-addon2" 
                        name="Email" value={this.state.Email} 
                        onChange={this.handleChange}/>
                </div>
                <div className="input-group mb-3">
                    <input className="form-control" type="text" 
                        placeholder="Phone number" aria-label="Phone number" aria-describedby="basic-addon2" 
                        name="Phone" value={this.state.Phone}
                        onChange={this.handleChange}/>
                </div>
                <div className="input-group mb-3">
                    <input className="form-control" type="text" 
                        placeholder="Address" aria-label="Address" aria-describedby="basic-addon2" 
                        name="Address" value={this.state.Address} 
                        onChange={this.handleChange}/>                   
                </div>

                <button  className="btn btn-primary btn-sm" onClick={this.setUserData.bind(this)}>Send Data</button>
            </div>

        )
    }
}
export default Profile;
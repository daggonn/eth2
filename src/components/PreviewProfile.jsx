import React from 'react';

class PreviewProfile extends React.Component {
    constructor(props) {
        super(props)
    };
    render() {
        return(
            <div className="container mt-5">
                <div className="alert alert-danger" role="alert">
                  For using smart contract please top up your wallet and check balance again. If it is nonzero you will can see interface for working with contract.  
                </div>
            </div>
        )
    }
}

export default PreviewProfile;